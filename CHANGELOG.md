# Changelog

All notable changes to this project will be documented in this file.

The format is based on version 1.0.0 of [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to version 2.0.0 of [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Contents

- [Unreleased](#unreleased)

## [Unreleased][1]

## [0.0.2][2] - 2017-07-31

### Added

+ `unit-value` executable to return the unit value.

## [0.0.1][3] - 2017-07-30

### Added

+ Library module `UnitValue` with constant `unitValue`.

* * *

<small>

[1]: https://gitlab.com/schas002/unit-value/tree/HEAD
[2]: https://gitlab.com/schas002/unit-value/tree/v0.0.2
[3]: https://gitlab.com/schas002/unit-value/tree/v0.0.1

</small>

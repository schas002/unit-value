-- | The module for the unit value.
module UnitValue (unitValue) where

{- |
	The unit value.

	>>> unitValue
	()
-}
unitValue :: ()
unitValue = ()

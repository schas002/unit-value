module UnitValueSpec (spec) where

import UnitValue (unitValue)
import Test.Hspec

spec :: Spec
spec = do
    describe "unitValue" $ do
        it "should be the unit value" $ do
            unitValue `shouldBe` ()

# unit-value

![Powered by love... and awoo](https://img.shields.io/badge/powered_by_love...-and_awoo-ff69b4.svg)

> Yet another package for the unit value.

## Usage

```haskell
import UnitValue (unitValue)

unitValue -- => ()
```

### CLI

```sh
$ unit-value
()
```

## API

### module <a id="UnitValue">`UnitValue`</a>

The module for the unit value.

#### <a id="unitValue">`unitValue`</a> :: ()

The unit value.

```haskell
>>> unitValue
()
```

## Maintainer

- Andrew Zyabin - @schas002 - [@zyabin101@botsin.space](https://botsin.space/@zyabin101)

## Contribute

Absolutely! Every issue and PR is welcome.

Every contributor to this repository must follow the code of conduct, which is: don't be rude.

## License

[MIT](LICENSE) &copy; Andrew Zyabin
